import vk_api
import time
import json
from random import randint
from vk_api.longpoll import VkLongPoll, VkEventType
from Configuration import Configuration
import threading
import colorama
class VkBot(object):
    def __init__(self, login, password):
        self.__vk, self.__longpoll = self.__auth(login, password)
        self.__id = self.__vk.users.get()[0]['id']
        self.__getAlbum()
        self.__inService = []
        self.__delay = int(Configuration().get_data("Delay", "delay"))

        self.__readingInterval = json.loads(Configuration().get_data("LeaveUnread", "readingInterval"))
        self.__sendpicInterval = json.loads(Configuration().get_data("SendPicture", "sendpicInterval"))

        self.__SendPicture = json.loads(Configuration().get_data("BlackList", "SendPicture"))
        self.__DeleteMessage = json.loads(Configuration().get_data("BlackList", "deleteMessage"))
        self.__LeaveUnread = json.loads(Configuration().get_data("BlackList", "leaveUnread"))

    def __auth(self, login, password):
        print(colorama.Fore.RED     + "GLOBAL BAN INCOMING")
        print(colorama.Fore.RED     + "--------------------------------------------")
        print(colorama.Fore.YELLOW  + "Authorization...")
        vk_session = vk_api.VkApi(login, password, captcha_handler=self.__captcha_handler)
        try:
            vk_session.auth(token_only=True)
            print(colorama.Fore.GREEN + "Authorization was successful!")
            return vk_session.get_api(), VkLongPoll(vk_session)
        except vk_api.AuthError as error_msg:
            print(colorama.Fore.RED + "Ты долбоеб! Вводи данные правильно, мудила, блядь: ")
            login, password = Configuration().set_login()
            return self.__auth(login, password)

    def __captcha_handler(self, captcha):
        key = input("Пошел на хуй {0}: ".format(captcha.get_url())).strip()

        return captcha.try_again(key)

    def __getName(self, event):
        if event.from_chat:
            return self.__vk.messages.getChat(chat_id=event.chat_id)['title']
        elif event.from_user and not event.from_me:
            user = self.__vk.users.get(user_ids=event.user_id)[0]
            return user['first_name'] + " " + user['last_name']


    def __getAlbum(self):
        print(colorama.Fore.YELLOW + "Getting the album")
        self.__album = json.loads(Configuration().get_data("SendPicture", "album"))
        if not self.__album['owner_id']:
            self.__album['owner_id'] = self.__id
        if not self.__album['album_id']:
            self.__album['album_id'] = "saved"
        self.__photos = self.__vk.photos.get(owner_id=self.__album['owner_id'], album_id=self.__album['album_id'], offset=0, rev=1)
        print(colorama.Fore.GREEN + "Got the album successfully!")

    def __getRandomPhoto(self):
        return "photo" + str(self.__album['owner_id']) + "_" + str(self.__photos["items"][randint(0, len(self.__photos["items"])-1)]['id'])


    def __readWithDelay(self, event, removeFromServiceAfter = False):
        try:
            waitTime = randint(int(self.__sendpicInterval[0]), int(self.__sendpicInterval[1]))
            print(colorama.Fore.BLUE + "The message from " + self.__getName(event) + " will be read in " + str(waitTime))
            time.sleep(waitTime)
            self.__vk.messages.markAsRead(peer_id=event.peer_id)
            print(colorama.Fore.GREEN + "Huita from " + self.__getName(event) + " has been read")
        except Exception as e:
            self.__vk.messages.send(peer_id=self.__id, message=str(e))
        finally:
            if removeFromServiceAfter:
                self.__inService.remove(event.peer_id)

    def __sendPicWithDelay(self, event, removeFromServiceAfter = False):
        try:
            self.__readWithDelay(event)
            waitTime = randint(int(self.__sendpicInterval[0]), int(self.__sendpicInterval[1]))
            print(colorama.Fore.BLUE + "The anime kartinka from " + self.__getName(event) + " will be read in " + str(waitTime))
            time.sleep(waitTime)
            self.__getRandomPhoto()
            self.__vk.messages.send(peer_id=event.peer_id, attachment=self.__getRandomPhoto())
            print(colorama.Fore.GREEN + "The anime kartinka from " + self.__getName(event) + " has been deployed sent")
        except Exception as e:
            self.__vk.messages.send(peer_id=self.__id, message=str(e))
        finally:
            if removeFromServiceAfter:
                self.__inService.remove(event.peer_id)


    def __eventService(self, event):
        if event.type == VkEventType.MESSAGE_NEW and event.to_me:
            peer_id = event.raw[3]
            print(colorama.Fore.CYAN + "New huita from " + self.__getName(event))

            if peer_id in self.__SendPicture:
                self.__inService.append(peer_id)
                threading.Thread(target=self.__sendPicWithDelay, args=[event, True]).start()
            elif peer_id in self.__LeaveUnread:
                self.__inService.append(peer_id)
                threading.Thread(target=self.__readWithDelay, args=[event, True]).start()
            elif event.user_id in self.__DeleteMessage:
                self.__vk.messages.delete(message_ids=event.raw[1])
                print(colorama.Fore.GREEN + "Message from " + self.__getName(event) + " has been deleted!")


    def start(self):
        print(colorama.Fore.RED + " STARTING ANTI-P.A.S.H.K.A. DEFENSE SYSTEM")

        for event in self.__longpoll.listen():
            if event.type == event.from_me:
                ''''
                   Костыли, чтобы меньше попадать на капчу        (не хуйня)
                '''
                time.sleep(self.__delay)


            if event.peer_id in self.__inService:
                print(colorama.Fore.BLUE + "Huita from " + self.__getName(event) + " will be ignored!")
                continue
            threading.Thread(target=self.__eventService, args=[event]).start()



           # time.sleep(self.__delay)




