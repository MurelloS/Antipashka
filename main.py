from Configuration import Configuration
from VkBot import VkBot
from threading import Thread
import colorama

def main():

    colorama.init(autoreset=True)
    print(colorama.Fore.RED + " STARTING ANTI-P.A.S.H.K.A. DEFENSE SYSTEM")
    login = Configuration().get_data("Account", "login")
    password = Configuration().get_data("Account", "password")

    if not(login and password):
        login, password = Configuration().set_login()

    VkBot(login, password).start()
if __name__ == '__main__':
    # todo восстановление файла настроек при повреждении

    Thread(target=main).start()
