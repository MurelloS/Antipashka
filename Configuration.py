import configparser
import json
class Configuration(object):
    def __init__(self):
        self.__conf = configparser.RawConfigParser()
        self.__conf.read("config.cfg")
    def set_data(self, section, variable, data):
        self.__conf.set(section, variable, data)
        self.__conf.write(open("config.cfg", "w"))
    def get_data(self, section, variable):
        return self.__conf.get(section, variable)

    def set_login(self, login="", password=""):
        if login and password:
            self.__conf.set("Account", "login", login)
            self.__conf.set("Account", "password", password)
        else:
            login = str(input("Enter the login: "))
            password = str(input("... and password: "))
            self.__conf.set("Account", "login", login)
            self.__conf.set("Account", "password", password)
            self.__conf.write(open("config.cfg", "w"))
            return login, password